local Class = require( "libraries/hump/class" )
local Entity = require( "entities/Entity" )


local checkpoint = Class{

    __includes = Entity -- Inherit from the Entity class

}


function checkpoint:newAnimation( image, width, height, duration )
    local animation = {}
    animation.spriteSheet = image
    animation.quads = {}

    for y = 0, image:getHeight() - height, height do
        for x = 0, image:getWidth() - width, width do
            table.insert( animation.quads, love.graphics.newQuad( x, y, width, height, image:getDimensions() ) )
        end
    end

    animation.duration = duration or 1
    animation.currentTime = 0

    return animation
end


function checkpoint:init( world, x, y, level )

    Entity.init( self, world, x, y, 32, 32 )
    self.world:add( self, self:getRect() )

    self.isCheckpoint = true
    self.level = level

    self.colorList = {
        { 0, 0, 1 },
        { 1, 1, 1 },
        { 0, 0.5, 0 },
        { 1, 0, 0 },
        { 0, 1, 0 }
    }

    self.animation = self:newAnimation( love.graphics.newImage( "assets/checkpoint-sheet.png" ), 32, 32, 1 )

end


function checkpoint:update( dt )

    -- Update the animation
    self.animation.currentTime = self.animation.currentTime + dt
    if self.animation.currentTime >= self.animation.duration then
        self.animation.currentTime = self.animation.currentTime - self.animation.duration
    end

end


function checkpoint:draw()

    local spriteNum = math.floor( self.animation.currentTime / self.animation.duration * #self.animation.quads ) + 1
    love.graphics.setColor( self.colorList[ self.level ] )
    love.graphics.draw( self.animation.spriteSheet, self.animation.quads[ spriteNum ], self.x, self.y )
    love.graphics.setColor( 1, 1, 1 )

end


return checkpoint
