local Class = require( "libraries/hump/class" )
local Entity = require( "entities/Entity" )


local player = Class{

    __includes = Entity -- Inherit from the Entity class

}


function player:newAnimation( image, width, height, duration )
    local animation = {}
    animation.spriteSheet = image
    animation.quads = {}

    for y = 0, image:getHeight() - height, height do
        for x = 0, image:getWidth() - width, width do
            table.insert( animation.quads, love.graphics.newQuad( x, y, width, height, image:getDimensions() ) )
        end
    end

    animation.duration = duration or 1
    animation.currentTime = 0

    return animation
end


function player:init( world, x, y, level )

    Entity.init( self, world, x, y, 32, 64 )

    -- Player variables
    self.xVelocity = 0
    self.yVelocity = 0
    self.acceleration = 50
    self.maxSpeed = 500
    self.friction = 20
    self.gravity = 80

    self.isJumping = false
    self.isGrounded = false
    self.hasReachedMax = false
    self.jumpAcceleration = 500
    self.jumpMaxSpeed = 11
    self.hasJumped = false
    self.jumpHeight = 0
    self.highestJump = 0
    self.level = level

    self.hasFinished = false

    self.world:add( self, self:getRect() )

    self.standing = self:newAnimation( love.graphics.newImage( "assets/character-standing-sheet.png" ), 32, 64, 1 )
    self.moving = self:newAnimation( love.graphics.newImage( "assets/character-moving-sheet.png" ), 32, 64, 0.5 )

end


function player:collisionFilter( other )

    if other.isCheckpoint then
        self.level = other.level
        return( "cross" )
    else
        return( "slide" )
    end

end


function player:update( dt )

    local prevX, prevY = self.x, self.y

    -- Friction
    self.xVelocity = self.xVelocity * ( 1 - math.min( dt * self.friction, 1 ) )
    self.yVelocity = self.yVelocity * ( 1 - math.min( dt * self.friction, 1 ) )

    -- Gravity
    self.yVelocity = self.yVelocity + ( self.gravity * dt )

    -- Keyboard
    if love.keyboard.isDown( "a", "left" ) and self.xVelocity > -self.maxSpeed then
        self.xVelocity = self.xVelocity - ( self.acceleration * dt )
    elseif love.keyboard.isDown( "d", "right" ) and self.xVelocity < self.maxSpeed then
        self.xVelocity = self.xVelocity + ( self.acceleration * dt )
    end

    if love.keyboard.isDown( "w", "up" ) then
        if -self.yVelocity < self.jumpMaxSpeed and not self.hasReachedMax then
            self.yVelocity = self.yVelocity - ( self.jumpAcceleration * dt )
        elseif math.abs( self.yVelocity ) > self.jumpMaxSpeed then
            self.hasReachedMax = true
        end
        self.isGrounded = false
        self.isJumping = true
    end

    -- Save the players predicted coordinates
    local goalX = self.x + self.xVelocity
    local goalY = self.y + self.yVelocity

    -- Move the world using bump.lua
    self.x, self.y, collisions, len = self.world:move( self, goalX, goalY, self.collisionFilter )

    -- Check the list of collisions to see what happened
    self.isGrounded = false
    for i, collision in ipairs( collisions ) do
        if collision.touch.y > goalY then
            self.hasReachedMax = true
            self.isGrounded = false
        elseif collision.normal.y < 0 then
            self.hasReachedMax = false
            self.isGrounded = true
            if self.isJumping == true then
                self.isJumping = false
                self.hasJumped = true
            end
            self.jumpHeight = 0
        end
    end

    -- Update the animation
    self.standing.currentTime = self.standing.currentTime + dt
    if self.standing.currentTime >= self.standing.duration then
        self.standing.currentTime = self.standing.currentTime - self.standing.duration
    end
    self.moving.currentTime = self.moving.currentTime + dt
    if self.moving.currentTime >= self.moving.duration then
        self.moving.currentTime = self.moving.currentTime - self.moving.duration
    end

    -- Increase the jump height
    if self.isGrounded == false then
        self.jumpHeight = self.jumpHeight + ( 10 * dt )
        if self.jumpHeight > self.highestJump then
            self.highestJump = self.jumpHeight
        end
    end

end


function player:draw()

    if math.floor( math.abs( self.xVelocity ) ) == 0 then
        local spriteNum = math.floor( self.standing.currentTime / self.standing.duration * #self.standing.quads ) + 1
        love.graphics.draw( self.standing.spriteSheet, self.standing.quads[ spriteNum ], self.x, self.y )
    elseif self.xVelocity / math.abs( self.xVelocity ) == -1 then
        local spriteNum = math.floor( self.moving.currentTime / self.moving.duration * #self.moving.quads ) + 1
        love.graphics.draw( self.moving.spriteSheet, self.moving.quads[ spriteNum ], self.x + 32, self.y, 0, math.abs( self.xVelocity ) / self.xVelocity, 1 )
    else
        local spriteNum = math.floor( self.moving.currentTime / self.moving.duration * #self.moving.quads ) + 1
        love.graphics.draw( self.moving.spriteSheet, self.moving.quads[ spriteNum ], self.x, self.y )
    end

end


return player
