local Class = require( "libraries/hump/class" )
local Entity = require( "entities/Entity" )


local Ground = Class{

    __includes = Entity -- Inherit from the Entity class

}


function Ground:init( world, x, y, width, height )

    Entity.init( self, world, x, y, width, height )

    self.world:add( self, self:getRect() )

end


function Ground:draw()

    love.graphics.setColor( 1, 1, 1 )
    love.graphics.rectangle( "fill", self:getRect() )

end


return Ground
