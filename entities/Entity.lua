-- This represents a single drawable object
local Class = require( "libraries/hump/class" )

local Entity = Class{}


function Entity:init( world, x, y, width, height )

    self.world = world
    self.x = x
    self.y = y
    self.width = width
    self.height = height

end


function Entity:getRect()

    return self.x, self.y, self.width, self.height

end


function Entity:draw()

    -- Defaults as empty

end


function Entity:update( dt )

    -- Defaults as empty

end


return Entity
