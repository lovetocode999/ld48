-- Import libraries
local Gamestate = require( "libraries/hump/gamestate" )
local Class = require( "libraries/hump/class" )
local shack = require( "libraries/shack/shack" )


-- Import the base level
local levelBase = require( "gamestates/levelBase" )


-- Import specific entities
local Player = require( "entities/player" )
local Checkpoint = require( "entities/endCheckpoint" )
local camera = require( "libraries/camera" )


-- Variable declarations
player = nil
local hasJumped = false
local level = 1

local gameLevel1 = Class{

    __includes = levelBase

}


function gameLevel1:init()

    levelBase.init( self, "assets/levels/level1.lua" )

end


function gameLevel1:enter()

    player = Player( self.world, 32, 64, 1 )
    checkpoint = Checkpoint( self.world, 960, 256, 2 )
    levelBase.Entities:add( checkpoint )
    levelBase.Entities:add( player )

end


function gameLevel1:update( dt )

    -- Update shack
    shack:update( dt )

    self.map:update( dt )
    levelBase.Entities:update( dt )
    levelBase.positionCamera( self, player, camera )
    if player.level ~= level then
        level = player.level
        levelBase.init( self, "assets/levels/level" .. level .. ".lua" )
        player = Player( self.world, 32, 64, level )
        if level ==  1 then
            checkpoint = Checkpoint( self.world, 960, 256, 2 )
            levelBase.Entities:add( checkpoint )
        elseif level == 2 then
            checkpoint = Checkpoint( self.world, 960, 336, 3 )
            levelBase.Entities:add( checkpoint )
        elseif level == 3 then
            checkpoint1 = Checkpoint( self.world, 960, 64, 1 )
            checkpoint2 = Checkpoint( self.world, 960, 160, 4 )
            checkpoint3 = Checkpoint( self.world, 960, 256, 5 )
            levelBase.Entities:addMany( { checkpoint1, checkpoint2, checkpoint3 } )
        end
        levelBase.Entities:add( player )
    end

end


function gameLevel1:draw()

    local width, height = love.graphics.getWidth(), love.graphics.getHeight()
    if level ~= 4 then
        camera:set()
        shack:apply()
        if level == 1 or level == 5 then
            love.graphics.setColor( 0.2, 0.2, 0.2 )
            love.graphics.rectangle( "fill", 0, 0, 40 * 32, 15 * 32 )
            love.graphics.setColor( 1, 1, 1 )
        elseif level == 2 then
            love.graphics.setColor( 0.2, 0.2, 0.2 )
            love.graphics.rectangle( "fill", 0, 0, 40 * 32, 15 * 32 )
            love.graphics.setColor( 0.2235, 0.4647, 0.5687, 0.2 )
            love.graphics.rectangle( "fill", 0, 0, 40 * 32, 15 * 32 )
            love.graphics.setColor( 1, 1, 1 )
        elseif level == 3 then
            love.graphics.setColor( 0.2235, 0.4647, 0.5687 )
            love.graphics.rectangle( "fill", 0, 0, 80 * 32, 15 * 32 )
            love.graphics.setColor( 1, 1, 1 )
        end
        self.map:draw( -camera.x, -camera.y )
        levelBase.Entities:draw()
        camera:unset()
        if level == 5 then
            love.graphics.setColor( 1, 1, 1 )
            love.graphics.printf( "Unfortunately you have chosen the wrong portal, and will now be doomed to survive in this boring universe for the rest of your miserable existence.\nEnjoy your stay.", 0, ( height / 2 ) + ( height / 3 ), width, "center" )
        end
        if level == 3 and player.x > 704 and player.x < 800 and player.y < 192 then
            love.graphics.setColor( 0.2, 0.2, 0.2, 0.5 )
            love.graphics.rectangle( "fill", width / 4, ( height / 4 ) + 96, width / 2, ( height / 2 ) - 96 )
            love.graphics.setColor( 1, 1, 1 )
            love.graphics.printf( "Red for hope and what once was\n\nBlue for a chance to start anew\n\nGreen for time to pass with no meaning", width / 4, ( height / 4 ) + 128, width / 2, "center" )
        end
    else
        love.graphics.setColor( 0.2, 0.2, 0.2 )
        love.graphics.rectangle( "fill", 0, 0, width, height )
        love.graphics.setColor( 1, 1, 1 )
        love.graphics.printf( "You finished the game and got back home!\nThanks for playing!", 0, height / 2, width, "center" )
    end

end


function gameLevel1:keypressed( key )

    levelBase:keypressed( key )
     
    if ( level == 4 or level == 5 ) and key == "escape" then
        love.event.quit()
    end

end


return gameLevel1
