gameOver = Gamestate.new()


function gameOver:draw()

    local width, height = love.graphics.getWidth(), love.graphics.getHeight()
    love.graphics.setColor( 0.2, 0.2, 0.2 )
    love.graphics.rectangle( "fill", 0, 0, width, height )
    love.graphics.setColor( 1, 1, 1 )
    love.graphics.printf( "You finished the game and got back home!\nThanks for playing!", 0, height / 2, width, "center" )

end


function gameOver:keypressed( key )

    if key == "escape" then
        love.event.quit()
    end

end


return gameOver
