-- Bump.lua for collisions
bump = require( "libraries/bump/bump" )

-- Shack.lua for cool screen shaking stuff
shack = require( "libraries/shack/shack" )

-- Gamestate support
Gamestate = require( "libraries.hump.gamestate" )


-- Initialize gamestates
level1 = require( "gamestates/level1" )


-- Initialize global variables
player = nil


function love.load()

    Gamestate.registerEvents()
    Gamestate.switch( level1 )

    music = love.audio.newSource( "assets/music.mp3", "static" )
    music:setLooping( true )
    love.audio.play( music )

end
